datalife_party_address_province
===============================

The party_address_province module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-party_address_province/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-party_address_province)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
